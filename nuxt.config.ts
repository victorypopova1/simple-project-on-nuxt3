// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {
    enabled: true
  },
  devServer: {
    port: 4200
  },
  css: ["bootstrap/dist/css/bootstrap.min.css"],
  runtimeConfig: {
    public: {
      baseURL: 'http://localhost:3000/api'
    }
  },
})
