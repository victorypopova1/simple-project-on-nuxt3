const BASE_URL = 'http://localhost:3000/api';

// функции для запросов для клиентского рендеринга
export async function getFetch(url: string, options?: RequestInit) {
    const response = await fetch(`${BASE_URL}${url}`, options);
    if (!response.ok) {
        throw await getErrorMessage(response);
    }
    const responseData = await response.clone().json();
    console.log(responseData)
    return responseData;
}

export async function postFetch(url: string, data: any) {
    const response = await fetch(`${BASE_URL}${url}`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (!response.ok) {
        throw await getErrorMessage(response);
    }
    return response.json();
}

// функция для запросов данных для серверного рендеринга (для серверного используется useFetch, а не fetch)
export async function getUseFetch(url: string) {
    return await useFetch(`${BASE_URL}${url}`);
}

async function getErrorMessage(response: Response) {
    const errorData = await response.json();
    const errorMessage = errorData.message || 'Ошибка сети';
    return new Error(errorMessage);
}